#!/bin/bash

# install nix
sh <(curl -L https://nixos.org/nix/install) --no-daemon

# source nix
. ~/.nix-profile/etc/profile.d/nix.sh

# install packages
nix-env -iA \
    nixpkgs.zsh \
    nixpkgs.git \
    nixpkgs.fzf \
    nixpkgs.ripgrep \
    nixpkgs.tmux \
    nixpkgs.vim \
    nixpkgs.neovim \
    nixpkgs.bat \
    nixpkgs.antibody

# set zsh as default shell
command -v zsh | sudo tee -a /etc/shells
sudo chsh -s $(which zsh) $USER

cp zshrc ~/.zshrc
cp p10k.zsh ~/.p10k.zsh

cp zsh_plugins.txt ~/.zsh_plugins.txt
antibody bundle < zsh_plugins.txt > ~/.zsh_plugins.sh


